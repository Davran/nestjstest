## Installation

```bash
$ npm install
```

## Running the app

```bash
# start
$ npm start
```

## Test

```bash
# unit tests
$ npm test
```

