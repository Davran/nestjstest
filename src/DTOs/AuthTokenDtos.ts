export interface AuthJWTTokenPayloadDto {
  role: string
  userId: string
  iat: number
}
