import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthJWTTokenPayloadDto } from '../../DTOs/AuthTokenDtos';
import { verifyAndDecodeToken } from '../../utils/jwt';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    const authToken = request.headers.authorization;
    if (!authToken) {
      return false
    }
    const decodedToken: AuthJWTTokenPayloadDto = verifyAndDecodeToken(authToken);
    return roles.some(it => it === decodedToken.role)
  }
}
