import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

describe('AuthController', () => {
  let app: TestingModule;
  let authController: AuthController;
  let authService: AuthService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
                                           controllers: [AuthController],
                                           providers: [AuthService],
                                         }).compile();
  });

  beforeEach(() => {
    authController = app.get<AuthController>(AuthController);
    authService = app.get<AuthService>(AuthService);
  });

  describe('Login_UserExists_JWTTokens', () => {
    it('should return access and jwt token', () => {
      const result = { accessToken: '', refreshToke: '' };
      jest.spyOn(authService, 'login').mockImplementation(() => result);
      expect(authController.login({ emailOrUsername: '', password: '' })).toBe(result);
    });
  });
});
