import { Injectable } from '@nestjs/common';
import { generateAuthTokens } from '../../utils/jwt';
import { AuthResponseDTO, LoginRequestDTO } from './auth.dtos';

@Injectable()
export class AuthService {
  login(dto: LoginRequestDTO): AuthResponseDTO {
    const role = 'guest';
    return generateAuthTokens('userId', role);
  }
}
