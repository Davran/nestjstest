export interface LoginRequestDTO {
  emailOrUsername: string;
  password: string;
}

export interface AuthResponseDTO {
  accessToken: string;
  refreshToken: string;
}
