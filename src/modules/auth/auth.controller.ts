import { Body, Controller, Get } from '@nestjs/common';
import { AuthResponseDTO, LoginRequestDTO } from './auth.dtos';
import { AuthService } from './auth.service';

@Controller('/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Get('login')
  login(@Body() dto: LoginRequestDTO): AuthResponseDTO {
    return this.authService.login(dto)
  }
}
