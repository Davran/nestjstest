import { Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { RolesGuard } from '../auth/guard';
import { Roles } from '../auth/roles.decorator';
import { UserService } from './user.service';

@Controller('/user')
export class UserController {
  constructor(private readonly userService: UserService) {

  }

  @Post()
  @UseGuards(RolesGuard)
  @Roles('admin')
  createOne(): string {
    return this.userService.createOne();
  }

  @Get(':id')
  @UseGuards(RolesGuard)
  @Roles('admin', 'guest')
  getOne(@Param('id') id: string): string {
    return this.userService.getOne(id);
  }

}
