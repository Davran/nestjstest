import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let app: TestingModule;
  let userController: UserController;
  let userService: UserService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
                                           controllers: [UserController],
                                           providers: [UserService],
                                         }).compile();
  });

  beforeEach(() => {
    userController = app.get<UserController>(UserController);
    userService = app.get<UserService>(UserService);
  });

  describe('GetOne_Exists_ReturnSuccessString', () => {
    it('should return success string', () => {
      const result = 'User found';
      jest.spyOn(userService, 'getOne').mockImplementation(() => result);
      expect(userController.getOne('mockedUserId')).toBe(result);
    });
  });

  describe('CreateOne_Exists_ReturnSuccessString', () => {
    it('should return success string', () => {
      const result = 'Created';
      jest.spyOn(userService, 'createOne').mockImplementation(() => result);
      expect(userController.createOne()).toBe(result);
    });
  });
});
