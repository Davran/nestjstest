import { HttpException, HttpStatus } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

const singAuthAccessToken = (userId: string, role: string) => {
  const payload = {
    userId,
    role,
    iat: Math.floor(Date.now() / 1000) - 30,
  };
  const signOptions = {
    expiresIn: Math.floor(Date.now() / 1000) + (60 * 60),
  };
  return jwt.sign(payload, 'secretSalt', signOptions);
};

const singAuthRefreshToken = () => {
  const payload = {
    iat: Math.floor(Date.now() / 1000) - 30,
  };
  const signOptions = {
    expiresIn: Math.floor(Date.now() / 1000) + (60 * 60),
  };
  return jwt.sign(payload, 'secretSalt', signOptions);
};

const generateAuthTokens = (userId: string, role: string) => {
  return {
    accessToken: singAuthAccessToken(userId, role),
    refreshToken: singAuthRefreshToken(),
  };
};

const verifyAndDecodeToken = (token: string): any => {
  try {
    const tokenWithoutPrefix = getJWTTokenWithoutPrefix(token);
    return jwt.verify(tokenWithoutPrefix, 'secretSalt');
  } catch (e) {
    throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
  }
};

const getJWTTokenWithoutPrefix = (token: string): string => {
  const prefix = 'Bearer';
  return token.replace(`${prefix} `, '');
};

export {
  generateAuthTokens,
  verifyAndDecodeToken,
};
